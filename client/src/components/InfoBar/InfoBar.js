import React from 'react';
import './InfoBar.css';
import onlineIcon from '../../images/onlineIcons.png';
import closeIcon from '../../images/closeIcons.png';
const InfoBar = ({room}) => (
    <div className="infoBar">
        <div className="leftInnerContainer">
            <img className="onlineIcon" src={onlineIcon} />
            <h3>{room}</h3>
        </div>
        <div className="rightInnerContainer">
            <a href="/"><img src={closeIcon} className="closeIcon"/></a>
        </div>
    </div>
)

export default InfoBar;