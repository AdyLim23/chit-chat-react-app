import React,{useState} from 'react';
import {Link} from 'react-router-dom';
import './Join.css';

const Join = () =>{
    const [name,setName] = useState('')
    const [room,setRoom] = useState('')

    return(
        <div className="joinOuterContainer">
            <div className="joinInnerContainer">
                <h1 className="heading">Chat Application</h1>
                <div><input placeholder="name" onChange={(e)=>setName(e.target.value)} type="text" className="joinInput"/></div>
                <div><input placeholder="room" onChange={(e)=>setRoom(e.target.value)} type="text" className="joinInput"/></div>
                {/* event.preventDefault() is used to disable when there is empty name and room */}
                <Link to={'/chat?name='+name+"&room="+room} onClick={event => (!name || !room) ? event.preventDefault() : null}>
                    <button type="submit" className="button mt-20" >Join</button>
                </Link>
                            
            </div>
        </div>
    )
}

export default Join;