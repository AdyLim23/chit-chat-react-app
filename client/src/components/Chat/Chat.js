import React,{useState,useEffect} from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';
import './Chat.css';
import InfoBar from '../InfoBar/InfoBar';
import Input from '../Input/Input';
import Messages from '../Messages/Messages';

let socket;

//queryString is used to get the params of the url ,JUST LIKE req.params ,get the params of the name and room input by user and pass to chat page
//location is KEYWORD and location.search
const Chat = ({location}) =>{
    const [name,setName] = useState('')
    const [room,setRoom] = useState('')
    //every time key in message
    const [message,setMessage] = useState('')
    //the whole message
    const [messages,setMessages] = useState([])
    const ENDPOINT = "https://chit-chat-react-application.herokuapp.com/"
    // const ENDPOINT = "localhost:5000"

    //init
    useEffect(()=>{
        const {name,room} = queryString.parse(location.search)
        console.log(name)
        setName(name)
        setRoom(room)
        //use io to connect
        socket = io(ENDPOINT)
        
        //emit means send    | user send info from front end to back end
        //{name,room} actually means {name:name,room:room}
        socket.emit('join',{name,room} ,()=>{
            //error receive message from server
        })
        //socket.emit 了后，在backend socket.on叫 这个'join' event

        //useEffect must have return statement !!!
        return()=>{
            socket.emit("disconnect")
            //socket off is necessary
            socket.off()
        }

      //only when ENDPOINT is changed or location.search is changed then the useEffect will rerender again !!!  
    },[ENDPOINT,location.search])


    useEffect(()=>{
        socket.on("message",(message)=>{
            setMessages([...messages,message])
        })
    },[messages])
    console.log(message,messages)
    const sendMessage = (event) => {
        //use the preventDefault here is due to don wan refresh the whole screen
        event.preventDefault();
        if(message){
            //callback here setMessage('') empty string due to need to clean the input after every sent
            //callback is run when the index.js is callback() which means setMessage empty string
            socket.emit("sendMessage",message , ()=>setMessage(''))
        }
    }
    // () for interface , {} for logic and define variable
    return(
        <div className="outerContainer">
            <div className="container">
                <InfoBar room={room}/>
                <Messages messages={messages} name={name}/>
                <Input message={message} setMessage={setMessage} sendMessage={sendMessage}/>
            </div>
        </div>
    )
}

export default Chat;