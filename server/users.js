 const users = [];

 //define function = () => {} GOT CURLY BRACKET
 //after click join button will call addUser
 const addUser = ({id,name,room}) => {
     //trim function is used to remove the whitespace between words ,make them combine together
    name = name.trim()
    room = room.trim().toLowerCase()
    //map function array.map(()=> )
    //(()=>{return})
    const existingUser = users.find((user) =>
        user.name === name && user.room === room
    )

    if(existingUser){
        return {error:"Username is taken"}
    }

    const user = {id,name,room}
    console.log("here87:",user)
    users.push(user);
    //q :    {id:'',name:'' ,room:''}
    console.log("q:",user)
    //w :    {user:{id: '' ,name:'' ,room: ''}}
    //so means {user} is object format and named as user
    console.log("w:",{user})

    return {user}
 }

 const removeUser = (id) => {
    const index = users.findIndex((user) => user.id === id);
    //index is 0
    console.log(users)
    console.log(index)
    
    console.log(users[0])
    if(index !==-1){
        return users.splice(index,1)
    }
 }

 const getUser = (id) => {
    var specificUser = users.find((user) => 
        user.id === id
    )
    return specificUser
 }

 const getUserInRoom = (room) => {
    return users.filter((user) =>
       user.room === room
    )
 }

 module.exports = {addUser,removeUser,getUser,getUserInRoom}